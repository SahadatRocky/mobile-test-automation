
let chai = require("chai")
const allure = require('allure-commandline')
exports.config = {
    runner: 'local',
    port: 4723,
    host: 'localhost',
    path: '/wd/hub',
    logLevel: 'info',
    framework: 'mocha',
    // Options to be passed to Mocha.
    // See the full list at: http://mochajs.org
    mochaOpts: {
        ui: 'bdd',
        require: ['@babel/register'],
        timeout: 600000
    },
    maxInstances: 1,
    sync: true,
    specs: [
        // './PlayWithMocha.js'
        './api/click.js'
    ],

    reporters: [
        ['allure', {
            outputDir: 'allure-results',
            disableWebdriverStepsReporting: true,
            disableMochaHooks: true
        }]
    ],

    capabilities: [
        // {
        // "platformName": "Android",
        // "automationName": "UiAutomator2",
        // "udid": "emulator-5554",
        // "appPackage": "com.wdiodemoapp",
        // "appActivity": ".MainActivity"
        // },

        {
        "platformName": "Android",
        "automationName": "UiAutomator2",
        "udid": "emulator-5554",
        "appPackage": "com.bat.insight",
        "appActivity": "com.bat.insight.ui.activity.SplashActivity"
        }
        
    ],

    before: function(){
        global.chaiExpect = chai.expect

        browser.addCommand('waitAndClick',function(selector){
            try{
                selector.waitForExist();
                selector.click();

            }catch(error){
               throw new Error(`could not click on selector: ${selector}`);
            }
        }); 
        
        browser.addCommand('waitAndTypeText',function(selector,text){
            try{
                selector.waitForExist();
                selector.setValue(text);

            }catch(error){
               throw new Error(`could not typeText on selector: ${selector}`);
            }
        });

    },

    onComplete: function() {
        const reportError = new Error('Could not generate Allure report')
        const generation = allure(['generate', 'allure-results', '--clean'])
        return new Promise((resolve, reject) => {
            const generationTimeout = setTimeout(
                () => reject(reportError),
                5000)

            generation.on('exit', function(exitCode) {
                clearTimeout(generationTimeout)

                if (exitCode !== 0) {
                    return reject(reportError)
                }

                console.log('Allure report successfully generated')
                resolve()
            })
        })
    },

    afterTest: function (test, context, { error, result, duration, passed, retries }) {
        if(error){
            browser.takeScreenshot()
        }
    }
}