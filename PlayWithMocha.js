

describe('features', () => {

    before( ()=> {
        console.log("Before All Test Case");
    });

    after( ()=> {
        console.log("After All Test Case");
    });

    beforeEach( ()=> {
        console.log("Before Each Test Case");
    });

    afterEach(()=> {
        console.log("After Each Test Case");
    });

     it('First test Case', ()=> {
        console.log("First test Case");  
     });

     it('Second test Case', ()=> {
        console.log("Second test Case");  
     });
});