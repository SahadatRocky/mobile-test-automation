# android-insightdb-app-test-automation

## environment variable
1. JAVA_HOME-Program Files\Java\jdk Path
2. ANDROID_HOME- \AppData\Local\Android\Sdk Path
3. Path - \AppData\Local\Android\Sdk\platform-tools,\AppData\Local\Android\Sdk\tools

## installation
1. visual stdio
2. NodeJS
3. latest appium
4. vysor
5. android stdio or install emulator
6. download android platform-tools

## depandancies
1.  npm i --save-dev webdriverio 
2.  npm i --save-dev @wdio/cli
3.  npm i --save-dev @wdio/local-runner
4.  npm i --save-dev @wdio/sync
5.  npm i --save-dev @wdio/mocha-framework
6.  npm i --save-dev @babel/cli
7.  npm i --save-dev @babel/core
8.  npm i --save-dev @babel/preset-env
9.  npm i --save-dev @babel/register
10. npm i --save-dev @wdio/allure-reporter
11. npm i --save-dev chai
12. npm i allure-commandline

## apk install to android
.\adb -s deviceNumber install AppName.apk

## find install package activity
.\adb shell pm list packages -f

## test Run
npm test run ./test-config.js